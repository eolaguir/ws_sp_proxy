#!/usr/bin/env python
from cgi import parse_qs, escape

from pybase.util.log import logDebug

import alm_api_operation as aao # mmi_name

def application( environ, start_response):

	status = '200 OK'

	logDebug( "input of application: %s", environ['REQUEST_URI'])
	
	try:
		request_body_size = int( environ.get( 'CONTENT_LENGTH', 0))

	except( ValueError):
		request_body_size = 0

	# When the method is POST the query string will be sent
	# in the HTTP request body which is passed by the WSGI server
	# in the file like wsgi.input environment variable.
	d = parse_qs( environ['QUERY_STRING'])

	SMS    = d.get( 'SMS',    [''])[0] # Returns the first SMS value.
	MSISDN = d.get( 'MSISDN', [''])[0] # Returns the first MSISDN value.
	SMSC   = d.get( 'SMSC',   [''])[0] # Returns the first SMSC value

	# Always escape input to avoid script injection
	SMS    = escape( SMS)
	MSISDN = escape( MSISDN)
	SMSC   = escape( SMSC)

	status = aao.download_applet( SMS, MSISDN, SMSC)

	response_body = """
	"""

	logDebug( "Response status: %s", status)
	start_response( status, [('Content-type','text/html')])

	return [response_body]

