#!/usr/bin/env python
import sys, os

from pybase.util.config import Cfg
from pybase.util.log import logInit,\
			    logError,\
			    logWarning,\
			    logInfo,\
			    logDebug,\
			    logSetPrefix

import alm
from alm_tools import NB_MAX_APPLI

from utils import toAscii, createDicoFromCfgFile 

simphonic = os.environ['SIMphonIC']
asp_name  = os.environ['ASP_NAME']
mmi_name  = "ws_sp_proxy"
dico_file = "alm_appli_prefix.cfg"

cfgPath = simphonic + "/asp/" + asp_name + "/mmi/" + mmi_name + "/cfg/"
cfg     = Cfg( cfgPath + "mmi_" + mmi_name + ".cfg", None)

logInit( "logWSGI", cfg['LOG_PATH'])

def almDownloadApplet( msisdn, aid):
	logInfo( "Sending apdu")
	media          = alm.almMedia_OTA
	execution_date = '0000/00/00'
	execution_hour = '00:00:00'
	clientID       = 12
	priority       = alm.almAppliPrio_MEDIUM
	add_op         = alm.almAddReqOpNew( clientID)
        vp             = 30000
	logInfo( "download applet with the aid %s for the msisdn %s", aid, msisdn)
	logDebug( "add_op=alm.almAddReqOpNew( %d)", clientID)
	logDebug( "alm.almDownloadAppli( '%s', add_op, '%s', %d, '%s', '%s', %d, %d)", msisdn, aid, media, execution_date, execution_hour, priority, vp)
	# ALM <= 3.3.3 api almDownloadAppli takes exactly 7 arguments
	# ALM >= 3.3.4 api almDownloadAppli takes exactly 8 arguments
	code, requestID = alm.almDownloadAppli( msisdn, add_op, aid, media, execution_date, execution_hour, priority)#, vp)
	if code!=0:
		logError( "ALM almDownloadAppli failed (%s) for MSISDN: %s", alm.almStatusToString( code), msisdn)
	else:
		logInfo( "request applet sent")

	return code, requestID

def almGetAvailableAppli( msisdn):
	logDebug( "alm.almGetAvailAppli( '%s', %d)", msisdn, NB_MAX_APPLI)
	code, appliList = alm.almGetAvailAppli( msisdn, NB_MAX_APPLI)
	if code!=0:
		logError( "ALM almGetAvailableAppli failed (%s) for MSISDN: %s", alm.almStatusToString( code), msisdn)

	return  code, appliList

def almGetLoadedAppliList( msisdn):
	logDebug( "alm.almGetLoadedAppli( '%s', %d)", msisdn, NB_MAX_APPLI)
	code, appliList = alm.almGetLoadedAppli( msisdn, NB_MAX_APPLI)
	if code!=0:
#		if code == 3:
#			l_pid = os.getpid()
#			l_cmd = "kill -4 " + str( l_pid)
#			os.system( l_cmd)
		logError( "ALM almGetApplicationAppli failed (%s) for MSISDN: %s", alm.almStatusToString( code), msisdn)

	return code, appliList

def createAppli2DownloadList( msisdn, almAppliPrefix):
	app_2downl_name_list = []
	app_2downl_list = []

	code, loaded_list = almGetLoadedAppliList( msisdn)
	if code==0:
		code, avail_list = almGetAvailableAppli( msisdn)
	if code==0:
		for appli_avail in avail_list:
			if appli_avail.name.find( almAppliPrefix)==0:
				isLoaded = False
				for appli_loaded in loaded_list:
					if appli_avail.aid==appli_loaded.aid:
						isLoaded = True
						break
				if isLoaded==False:
					app_2downl_name_list.append( appli_avail.name)
					app_2downl_list.append( appli_avail.aid)
				break
	if len( app_2downl_list)>0:
		logDebug( "%d application(s) to download: %s %s", len( app_2downl_list), app_2downl_name_list, app_2downl_list)

	return code, app_2downl_list

#def haveApplet( msisdn, aids):
#	code, app_list = almGetLoadedAppliList( msisdn)
#	if code != 0:
#			return 1, 0, ''
#	for applidec in app_list:
#		logDebug( " item in the card " + str( applidec.aid))
#		if applidec.aid in aids:
#			return 0, 1, applidec.aid
#	return 0, 0, ''

#def getAid( msisdn, aids):
#	code, app_list = almGetAvailableAppli( msisdn)
#	if code!=0:
#		logError( "get availableappli failed" + str(code) + "MSISDN %s " %( msisdn))
#		return 1, ""
#	for applidec in app_list:
#		logDebug( "item can be downladed " + str( applidec.aid))
#		if applidec.aid in aids:
#			return 0, applidec.aid
#	return 0, ""

def almInit():
#	alm_server, alm_port = mmi_tools.getAdmCfgParam()
	alm_server = cfg['ALM_SERVER']
	alm_port   = cfg['ALM_PORT']
	try:
		logDebug( "import alm")
		logDebug( "alm.almInitAPI( '" + alm_server + "', int( " + alm_port + "))")
		st = alm.almInitAPI( alm_server, int( alm_port))
	except ValueError:
		logError( "Bad Argument Value for Admin Initialisation")
		sys.exit( 1)

	if st!=0:
		logError( "Cannot initialise ALM APIs: %s", alm.almStatusToString( st))
	return st

def almConnect():
	alm_login    = cfg['ALM_LOGIN']
	alm_password = cfg['ALM_PASSWORD']
	try:
		logDebug( "alm.almConnect( '" + alm_login + "', '" + alm_password + "')")
		st = alm.almConnect( alm_login, alm_password)
		if st==2:	#almErr_ALREADY_CONNECTED
			st = 0	#almOK
	except ValueError:
		logError( "Bad Argument Value for Admin Connection")
		sys.exit( 1)

	if st!=0 and st!=2:
		logError( "Cannot connect to ALM: %s", alm.almStatusToString( st))
	return st

def almGetConnectParameters():
	st, usrid, sk = alm.almGetConnectParameters()
	return (st, usrid, sk)

def almDisconnect():
	logDebug( "alm.almDisconnect()")
	st = alm.almDisconnect()

	if st!=0:
                logError( "Cannot disconnect from ALM: %s", alm.almStatusToString( st))
        return st

def download_applet( SMS, MSISDN, SMSC):

	message_treatment = True
	status            = '200 OK'

	try:
		logDebug( "Create dico from file : %s", cfgPath + dico_file)
		almAppliDico = createDicoFromCfgFile( cfgPath + dico_file)
		logDebug( "Available ALM appli prefix: %s", almAppliDico)

	except KeyError, e:
		logError( e.__str__())
		message_treatment = False
		status            = '200 OK'
	except:
		logError( "Parsing error of Dico")
		message_treatment = False
		status            = '200 OK'

	# Remove the : from the MSISDN and SMSC
	SMS = toAscii( SMS)
	SMS = SMS.split( "||")

	MSISDN = MSISDN.split(":")
	MSISDN = MSISDN[0]
	if MSISDN[0:1] == " ":
		MSISDN = MSISDN.replace( " ", "+", 1)
#	logSetPrefix( 'msisdn', MSISDN)

	SMSC = SMSC.split( ":")
	SMSC = SMSC[ min( 1, len( SMSC)-1)] # protection against out-of-range
	
	logDebug( "MSISDN: %s",                    MSISDN)
	logDebug( "Details on SMSC: content: %s",  SMSC)
	logDebug( "Details on SMSC: len(%d)", len( SMSC))
	logDebug( "Details on SMS: content: %s",   SMS)
	logDebug( "Details on SMS: len(%d)",  len( SMS))
	logDebug( "Details on SMS: position of Application ID: SMS[%d]", min( 2, len( SMS)-1))

	try:
		if( message_treatment == True):
			msisdnFromMessage = SMS[0]
			moKeyword         = SMS[ min( 1, len( SMS)-1)].lower()	# protection against out-of-range

 			if( msisdnFromMessage == ""):
				logError( "MSISDN is missing")
				message_treatment = False
				status            = '200 OK'
			else:
				if( len( SMS) == 1): # there is only application id in SMS
					msisdnFromMessage = MSISDN
				else:
					logDebug( "MISDN from message = %s", msisdnFromMessage)
				usedMobileId = msisdnFromMessage

			if( moKeyword == ""):
				logError( "ALM appli prefix is not defined")
				message_treatment = False
				status            = '200 OK'
			else:
				logDebug( "Search in dico for moKeyword [%s]", moKeyword)
				#Get the ALM appli prefix from SMSC
				almAppliPrefix = almAppliDico[moKeyword][0]
				logInfo( "ALM appli prefix from message = %s", almAppliPrefix)

	except KeyError, e:
		logError( "Error : [Undefined entry %s]", str( e))
		message_treatment = False
		status            = '200 OK'
	except:
		logError( "Wrong message format or missing identifier")
		message_treatment = False
		status            = '200 OK'

	try:
		# connection to ALM using API
		if( message_treatment == False or almInit() or almConnect()):
			message_treatment = False
			status            = '200 OK'

		if( message_treatment == True):

			st, l_aid = createAppli2DownloadList( usedMobileId, almAppliPrefix)

			if st!=0:
				logError( "ALM error : unable to check if applet [%s] exists for msisdn [%s]", almAppliPrefix, usedMobileId)
			elif len( l_aid)==0:
				logInfo( "No new applet [%s] to download for msisdn [%s]", almAppliPrefix, usedMobileId)
			else:
				logInfo( "Download applets [%s] for msisdn [%s]", almAppliPrefix, usedMobileId)
				st = almDownloadApplet( usedMobileId, l_aid[0])

		st = almDisconnect()

	except Exception, e:
	 	logError( "Error : [%s]", str(e))
		status  = '200 OK'

	return status

if __name__=='__main__':
	#			   SMS   MSISDN     SMSC
        status = download_applet( "32", "+123456", "1234")

