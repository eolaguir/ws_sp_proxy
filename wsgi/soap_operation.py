#from client_log import LOG, CFG, Cfg
import sys
import httplib
import signal
import string
from pybase.util.config import *
from pybase.util.log import *
from binascii import b2a_hex as toAsc
from binascii import a2b_hex as toHex

def validIP(address):
    parts = address.split(".")
    if len(parts) != 4:
            return False
    for item in parts:
            if not 0 <= int(item) <= 255:
                    return False
    return True

class RequestSOAP:
    def __init__(self, msisdn, activationCode, MNC, CC):
        self.msisdn = msisdn
        self.activationCode = activationCode
	self.MNC = MNC
	self.CC = CC

    def header(self):
        return """
        HTTP/1.0 200 OK
Server: WSGI/1.1 BaseHTTP/0.3 Python/2.7
Content-type: text/xml; charset='utf-8'
        """

    def soap_body(self):
        return ""

    def serialize(self):
        return self.soap_body()

class RequestTelepathIC_PullingProcess_WebService(RequestSOAP):
    def __init__(self, msisdn, activationCode,MNC,CC):
        RequestSOAP.__init__(self, msisdn, activationCode,MNC,CC)
        
    def  soap_body(self):
        return """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tsm="http://convergence.oberthur.com/tsmsp/">
   <soapenv:Header/>
   <soapenv:Body>
      <tsm:TelepathIC_PullingProcess_WebService>
         <tsm:MSISDN>%s</tsm:MSISDN>
         <tsm:ActivationCode>%s</tsm:ActivationCode>
	 <tsm:MNC>%s</tsm:MNC>
         <tsm:CC>%s</tsm:CC>
      </tsm:TelepathIC_PullingProcess_WebService>
   </soapenv:Body>
</soapenv:Envelope>"""\
                %(self.msisdn,self.activationCode,self.MNC,self.CC)
                
class SERVER_HTTP_Exception(Exception):
    def __init__(self, name, cause):
            self.value = name
            self.cause = cause
    def __str__(self):
            if self.cause is not None:
                    return "%s [%s]"%(repr(self.value), self.cause.__str__())
            return repr(self.value)

class SERVER_HTTP_TimeOutException(SERVER_HTTP_Exception):
    def __init__(self, cause):
            SERVER_HTTP_Exception.__init__(self, "SERVER timeout", cause)

class SERVER_HTTP_RetryException(SERVER_HTTP_Exception):
    def __init__(self, val, cause):
            SERVER_HTTP_Exception.__init__(self, val, cause)

class SERVER_HTTP_Connection_Failure(SERVER_HTTP_RetryException):
    def __init__(self, cause):
            SERVER_HTTP_RetryException.__init__(self, "SERVER HTTP connection failure", cause)

class SERVER_HTTP_Sending_Failure(SERVER_HTTP_RetryException):
    def __init__(self, cause):
            SERVER_HTTP_RetryException.__init__(self, "SERVER HTTP sending failure", cause)

class SERVER_HTTP_Reception_Failure(SERVER_HTTP_RetryException):
    def __init__(self, cause):
            SERVER_HTTP_RetryException.__init__(self, "SERVER HTTP reception failure", cause)

class SERVER_RESPONSE_Error(SERVER_HTTP_Exception):
    def __init__(self, cause):
	    SERVER_HTTP_Exception.__init__(self, "Reponse error", cause)

class SERVER_HTTP_Answer_Failure(SERVER_HTTP_RetryException):
    def __init__(self, val, tryIdx):
            SERVER_HTTP_RetryException.__init__(self, "SERVER HTTP answer with code [%d] <> [200], Try #%d"%(val, tryIdx), None)

                
                
class ServerConnection():
    def __init__(self, url):
        self.server_url  = url
        self.request_id  = 0
        # Required params for httplib, to establish HTTP connection
        self.server_add  = ""
        self.server_port = 0
        self.urlpath     = ""
        self.setServerAddress_Elems()
        
    def setServerAddress_Elems(self):
        urlPath          = ''
        self.server_add  = ""
        self.server_port = 0
        try:
                _url = self.server_url
                beg  = (string.upper(self.server_url))[0:7]
                if beg == 'HTTP://':
                        _url = self.server_url[7:]
                offset          = string.index( _url, '/')
                self.urlpath    = _url[offset:]
                urlAddress      = _url[:offset]
                _addrParts      = string.splitfields(urlAddress, ':')
                self.server_add = _addrParts[0]
#if validIP(self.server_add) ==  False and self.server_add != "localhost":
#			logError("Configured URL [%s] has wrong IP"%self.server_url)
#			raise Exception()
                if len( _addrParts) == 1:
                        logDebug( "No port specified in URL. Use default 80 port.")
                        self.server_port = 80
                else:
                        self.server_port = int( _addrParts[1], 10)
                logDebug( "ip:%s port:%s path:%s",self.server_add, self.server_port, self.urlpath)
        except Exception, e:
	                logError( "Error during URL parsing: [%s]"%e.__str__())
        	        raise e
			
    def disconnect(self):
        if self.con is None:
		try:
			self.con.close()
		except Exception, h:
			logError("Connection closure failed [%s]",h.__str__())

    def sendRequestHTTP(self, soap_message):
        
	logDebug("Message to be sent to Server")
        
        self.con = None

        try:
            self.con = httplib.HTTPConnection(self.server_add,self.server_port)
            self.con.connect()
        except Exception, e1:
            raise SERVER_HTTP_Connection_Failure(e1)

        try:
            self.con.putrequest("POST", self.urlpath)
            self.con.putheader("Content-Type", "text/xml")
            self.con.putheader("Content-Length",str(len(soap_message)))
            self.con.endheaders()
            self.con.send(soap_message)
        except Exception, e2:
            self.disconnect()
            raise SERVER_HTTP_Sending_Failure(e2)
	
	logInfo("Message sent to Server")

        response = None
        try:
            response = self.con.getresponse()
            dataResponse = response.read()
            self.disconnect()
            logDebug("Response: %s\n%s", dataResponse, toAsc(dataResponse))
            l_IsInError = "false"
            try:
                f1="<IsInError>"
                f2="</IsInError>"
                begin=dataResponse.find(f1);
                end=dataResponse.find(f2);
                l_IsInError=dataResponse[begin+len(f1):end]
                logInfo("IsInError : "+l_IsInError)
                if (l_IsInError == "true"):
                    f1="<ErrorCode>"
                    f2="</ErrorCode>"
                    begin=dataResponse.find(f1);
                    end=dataResponse.find(f2);
                    l_errorCode=dataResponse[begin+len(f1):end]
                    f1="<ErrorMessage>"
                    f2="</ErrorMessage>"
                    begin=dataResponse.find(f1);
                    end=dataResponse.find(f2);
                    l_errorMessage=dataResponse[begin+len(f1):end]
                    logError("SP error response : code "+l_errorCode+" <"+l_errorMessage+">")
                    return '200 OK',l_errorMessage

                elif (l_IsInError == "false"):
                    logInfo("Service installation in progress")
            except:
                logError("Exception in parsing")
                l_IsInError = "false"

        except Exception, e3:
                logError("Server HTTP response received not valid (no HTTP code)")
                raise SERVER_HTTP_Reception_Failure(e3)

        logDebug("Message acknowledged by client")

        return '200 OK',''
        
        
if __name__=="__main__":    
	simphonic = os.environ['SIMphonIC']
	asp_name = os.environ['ASP_NAME']
	cfgPath = simphonic + "/asp/" + asp_name + "/mmi/ws_sp_proxy/cfg/"
	cfg = Cfg(cfgPath + "mmi_ws_sp_proxy.cfg",None)
	logInit("logWSGI","./")
	client_address = cfg['SOAP_TARGET']
	con = ServerConnection(client_address)        
	reqPul = RequestTelepathIC_PullingProcess_WebService("1234566","activation1",12,22)        
	soap_message = reqPul.serialize()        
	con.sendRequestHTTP(soap_message)                            


