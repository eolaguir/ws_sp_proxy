#!/usr/bin/env python
import sys
from pybase.util.log import *
from pybase.util.config import *

from cgi import parse_qs, escape
from utils import toAscii, createDicoFromCfgFile
from soap_operation import ServerConnection,RequestTelepathIC_PullingProcess_WebService\
				,SERVER_HTTP_Connection_Failure,SERVER_HTTP_Sending_Failure\
				,SERVER_HTTP_Reception_Failure,SERVER_HTTP_RetryException\
				,SERVER_HTTP_TimeOutException,SERVER_RESPONSE_Error,SERVER_HTTP_Exception


def application(environ, start_response):

   message_treatment = True
   status = ''
   error = ''

   simphonic = os.environ['SIMphonIC']
   asp_name = os.environ['ASP_NAME']
   cfgPath = simphonic + "/asp/" + asp_name + "/mmi/ws_sp_proxy/cfg/"

   cfg = Cfg(cfgPath + "mmi_ws_sp_proxy.cfg",None)

   logInit("logWSGI",cfg['LOG_PATH'])

   try:
	mnoIdDico = createDicoFromCfgFile( cfgPath + "mnoid_params.cfg")
   except KeyError, e:
	logError(e.__str__())
        message_treatment = False
        status = '200 OK'
	error=str(cfg['SP_HTTP_ERROR_1'])
   except:
	logError("Parsing error of MNOids")
	message_treatment = False
	status = '200 OK'
	error=str(cfg['SP_HTTP_ERROR_1'])

   logDebug("input of application: %s", environ['REQUEST_URI'])
   
   try:
      request_body_size = int(environ.get('CONTENT_LENGTH', 0))
   except (ValueError):
      request_body_size = 0

   # When the method is POST the query string will be sent
   # in the HTTP request body which is passed by the WSGI server
   # in the file like wsgi.input environment variable.
   d = parse_qs(environ['QUERY_STRING'])

   SMS    = d.get( 'SMS',    [''])[0] # Returns the first SMS value.
   MSISDN = d.get( 'MSISDN', [''])[0] # Returns the first MSISDN value.
   SMSC   = d.get( 'SMSC',   [''])[0] # Returns the first SMSC value

   # Always escape input to avoid script injection
   # Remove the : from the MSISDN and SMSC
   SMS = escape( SMS)
   SMS = toAscii( SMS)
   SMS = SMS.split( "||")

   MSISDN = escape( MSISDN)
   MSISDN = MSISDN.split(":")
   MSISDN = MSISDN[0]
   if MSISDN[0:3] == " 33":
   	logDebug("MSISDN: %s", MSISDN[0:3])
   	MSISDN = MSISDN.replace( " ", "+", 1)
   logSetPrefix( 'msisdn', MSISDN)

   SMSC = escape( SMSC)
   SMSC = SMSC.split( ":")
   SMSC = SMSC[ min( 1, len( SMSC)-1)] # protection against out-of-range
   
   logDebug("MSISDN: %s", MSISDN)
   logDebug("Details on SMSC: content %s",   SMSC)
   logDebug("Details on SMSC: len(%d)", len( SMSC))
   logDebug("Details on SMS: content: %s",   SMS)
   logDebug("Details on SMS: len(%d)",  len( SMS))
   logDebug("Details on SMS: position of activationCodeFromMessage: SMS[%d]", min( 2, len( SMS)-1))

   try:
	if(message_treatment == True):
		#Get the MnoId information from SMSC
		mnoIdParameters = mnoIdDico[SMSC]
		logDebug("Value of mnoids(%s): %s", SMSC, mnoIdParameters)

		msisdnFromMessage = SMS[0]
 		idTechFromMessage = SMS[0]
   		if( len( SMS) == 1): # this is activationCode
			msisdnFromMessage = MSISDN
			idTechFromMessage = MSISDN
		elif( len( SMS) == 3):
 			idTechFromMessage = SMS[1] # 3 args => second one
   		activationCodeFromMessage = SMS[ min( 2, len( SMS)-1)]   # protection against out-of-range

		logDebug("AWAIT_MOBILE_IDENTIFIER found : %s", cfg['AWAIT_MOBILE_IDENTIFIER'].lower())

		if( cfg['AWAIT_MOBILE_IDENTIFIER'].lower() == 'msisdn'):
	   		if (msisdnFromMessage == ""):
				logError("MSISDN is missing or AWAIT_MOBILE_IDENTIFIER is bad configured")
				message_treatment = False
				status = '200 OK'
				error=str(cfg['SP_HTTP_ERROR_1'])
			else:
				usedMobileId = msisdnFromMessage
				logDebug("MSISDN from message = %s", msisdnFromMessage)
		elif( cfg['AWAIT_MOBILE_IDENTIFIER'].lower() == 'idtech'):
   			if(idTechFromMessage == ""):
				logError("IDTECH is missing or AWAIT_MOBILE_IDENTIFIER is bad configured")
				message_treatment = False
				status = '200 OK'
				error=str(cfg['SP_HTTP_ERROR_1'])
			else:
				usedMobileId = idTechFromMessage
				logDebug("idTech from message = %s", idTechFromMessage)
		else:
			logError("AWAIT_MOBILE_IDENTIFIER is bad configured (msisdn or idtech)")
			message_treatment = False
			status = '200 OK'
			error=str(cfg['SP_HTTP_ERROR_1'])

	   	if (activationCodeFromMessage == ""):
			logError("Activation code empty")
			message_treatment = False
			status = '200 OK'
			error=str(cfg['SP_HTTP_ERROR_1'])
		else:
			logInfo("activationCode from message = %s", activationCodeFromMessage)

   except KeyError, e:
	logError("Error : [Undefined entry %s]", str(e))
	message_treatment = False
	status = '200 OK'
	error=str(cfg['SP_HTTP_ERROR_1'])
   except:
	logError("Wrong message format or missing identifier")
	message_treatment = False
	status = '200 OK'
	error=str(cfg['SP_HTTP_ERROR_1'])

 
   try:
   	if(message_treatment == True):
		soap_target = cfg['SOAP_TARGET']
   		con = ServerConnection(soap_target)
		reqPul = RequestTelepathIC_PullingProcess_WebService(usedMobileId,activationCodeFromMessage,mnoIdParameters[0],mnoIdParameters[1])
		soap_message = reqPul.serialize()
		logDebug("soap message: %s", soap_message)
		status,error = con.sendRequestHTTP(soap_message)
		logInfo("Status from sending request Http: %s", status)
		logInfo("Error: %s", error)

   except SERVER_HTTP_Connection_Failure, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   except SERVER_HTTP_Sending_Failure, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])
	
   except SERVER_HTTP_Reception_Failure, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   except SERVER_HTTP_RetryException, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   except SERVER_HTTP_TimeOutException, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   except SERVER_RESPONSE_Error, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   except SERVER_HTTP_Exception, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])
	
   except Exception, e:
   	logError("Error : [%s]", str(e))
	status = "501 Error"
	error=str(cfg['SP_HTTP_ERROR_1'])

   response_body = """"""
   logDebug( "Response status: %s", status)
   if len(error)!=0:
	response_body = """<MSISDN>"""+MSISDN+"""</MSISDN><USER>sg</USER><SMS>"""+str(error)+"""</SMS>"""
	logDebug( "Response body %s", response_body)
   start_response(status,[('Content-type','text/html')])
   return [response_body]
