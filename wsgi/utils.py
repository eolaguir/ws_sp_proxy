#!/usr/bin/env python
#******************************************************************************
#  file: $Id: utils.py,v 1.5 2012-11-05 14:24:22 rodridel Exp $
#  author:       Benjamin PORTIER
# -----------------------------------------------------------------------------
#  v1.0, 2012/06/08:    creation
# *****************************************************************************/

import binascii

def toAscii(part):
        i = 0
        out = ""
        while i < len(part)-1:
                out += chr((int(part[i:i+2], 16)))
                i+=2
        return out

def toHexa(asciiStr):
        out = ""
        for i in range(len(asciiStr)):
                out+="%.2X"%(ord(asciiStr[i]))
        return out


def createDicoFromCfgFile( pathToFile):

    fileHandle = open( pathToFile)
    line = fileHandle.readline()
    dico_current_entries = {}

    try:
        while line != '':
                if line[0]!='#':
                        splitLine = line.split('|')
			if len( splitLine)>1:
	                        key = str( splitLine[0]).lower()
        	                if key in dico_current_entries:
                	                raise KeyError( 'Error in dictionary: double entry in dictionary ' + pathToFile)
				dico_current_entries[key] = (splitLine[1].rstrip(),)
        	                for i in range( 2, len( splitLine)):
                	                dico_current_entries[key] = dico_current_entries[key] + (splitLine[i].rstrip(),)
                line = fileHandle.readline()
    except KeyError, keyerr:
    	raise keyerr
    except Exception, e:
	raise e

    return dico_current_entries

