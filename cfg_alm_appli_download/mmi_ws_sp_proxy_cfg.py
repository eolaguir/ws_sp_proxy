#!/usr/bin/env python

import os, sys, pwd
import commands
from mmi_configurator import CfgParameter

#is SIMphonIC defined ?
simphoni=os.getenv('SIMphonIC')
if not simphoni:
        print '$SIMphonIC not set'
        sys.exit(1)

#is HOME defined ?
home=os.getenv('HOME')
if not home:
	print '$HOME not set'
	sys.exit(1)

#ASP ?
asp=commands.getoutput('whoami')


log_path = CfgParameter(	
		name='LOG_PATH',
		prompteur='Log directory ...................... ',
		value='%s/asp/%s/mmi/proxy4_mtmoSRV/logs' % (simphoni,asp))
				
alm_server = CfgParameter(
                name='ALM_SERVER',
		prompteur='ALM server ip ? ....................... ',
		value='localhost')

alm_port = CfgParameter(
                name='ALM_PORT',
		prompteur='ALM server port ? ....................... ',
		value='5866')

alm_login = CfgParameter(
                name='ALM_LOGIN',
		prompteur='ALM login ? ....................... ',
		value='alm_admin')

alm_password = CfgParameter(
                name='ALM_PASSWORD',
		prompteur='ALM password ? ....................... ',
		value='password')

proxy4_mtmoSRV_config = [
		'proxy4_mtmoSRV parameters',
		log_path,
		alm_server,
		alm_port,
		alm_login,
		alm_password
		]

