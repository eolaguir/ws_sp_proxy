#! /usr/bin/env python

#-----------------------------------------------------------------------------
# file:		$Id: mmi_configurator.py,v 1.1 2012-10-31 09:45:32 rodridel Exp $
# project:	MMICORE
# ----------------------------------------------------------------------------
# Description:	mmi configurator
# ----------------------------------------------------------------------------

import sys,os, re, string
from time import strftime,localtime

# automatic tools that feed configuration from a dictionnary do no support
# raw_input without a flush
# besides, they need a '?' at the end, not before
def mmi_raw_input(prompt):
	prompt = prompt.replace("?", "")
	sys.stdout.write(prompt + "?")
	sys.stdout.flush()
	return raw_input('')

class MMIConfigurator:
	"""Generic mmi configurator.

	If you are not happy with the default definition of those parameters, 
	you can eiter override the private attribute '_parameters'
	or override the 'set_defaultparameters' method.
	You can add extra parameters thanks to the attribute extra_parameters,
	which must be a list of CfgParameter objects. The methods 
	'addParameters' and 'addParamList' are alternative ways of doing it. 
	Parameters are asked and written in the order they appear in the list. 
	If a string is found in the parameter list, it is written as a comment 
	('#') preceded by a blank line in the config file.
	The run method launches the mmi configurator: reading the old config 
	file if any, asking for new parameters values and writing the new 
	config file.
	"""

	def __init__(self,extra_parameters=[],**kw):
		self.extra_parameters = extra_parameters  #list of CfgParameter
		self.cfgfilename = None
		self._cfg_dir = None
		self._header = None
		self._invite = None
		self._farewell = None
		for name, value in kw.items():
	            setattr(self, name, value)
		self.set_defaultparameters()

	def set_defaultparameters(self):
		debug=CfgParameter(
			name='DEBUG_MODE',
			type='choice',
			choices = ['yes','no'],
			value='no',
			prompteur='switch on proxy4_mtmoSRV debug mode?')

		self._parameters = [
			'debug mode',debug ] + self.extra_parameters

	def addParamList(self,parameters):
		for p in parameters:
                        self._parameters.append(p)

	def addParameters(self,*parameters):
		for p in parameters:
			self._parameters.append(p)

	def edit(self):
		print "\nplease, enter the new values (default in []):"
		for p in self._parameters:
		  if type(p)!=type('string'): p.prompte()
		  else: print;print '# %s' % p

	def read_config(self,ifile):
		print "reading old config file ..."
		_read_param = []
		for l in ifile:
		  #print '**NEW LINE**' 
		  #print 'l=%s' % l
		  if _cmt_line.match(l): continue #comment found
		  if _blk_line.match(l): continue #blank line found
		  for p in self._clean_parameters:
		    if p.name in _read_param:
			#print '%s already found' % p.name
			continue	
		    m=p.match(l)
		    if m:	
			_read_param.append(p.name)
			break

		for p in self._clean_parameters:
		  if p.name not in _read_param:
			print '%s not found or invalid' % p.name
		    		
			
	def write_config(self,ofile):
		if self._header is None: # user can change it
			self._header = _HEADER % {
				'appliname':self.appliname,
				'aspname':self.aspname,
				'time':strftime('%a, %d %b %Y %H:%M:%S',
						localtime()) }
		ofile.write(self._header)
		ofile.write('\n\n')
		for p in self._parameters:
			if type(p)!=type('string'):
				ofile.write("%s\t%s" % (p.name,p.value))
			else: 
				ofile.write('\n')
				ofile.write('# %s' % p)
			ofile.write('\n')
		
	def syntax(self,argv):
		#for compatibility with scl but mmi_[mmi name] is useless
		print "*** scl and mmi configurator do not match: run the configurator manually ***"
		print "syntax: %s mmi_%s [asp name]" % (argv[0],self.appliname)

	def clean_parameters(self):
		self._clean_parameters=[]
		for p in self._parameters:
			if type(p)!=type('string'):
				self._clean_parameters.append(p)

	def set_appliaspname(self,argv):
		# retrieve appliname from configurator executable name
		# do it before test on argv length because syntax() needs it
		name_re = re.compile(r'mmi_(\w+)_cfg$')
		try:
		  self.appliname=name_re.search(argv[0]).group(1)
		except:
		  print 'failed to get appli name'
		  sys.exit(1)

		# arg 2 mmi_[mmi name] is useless, but scl dictates the law!
		if len(argv) != 3:
			self.syntax(argv)
			sys.exit(1)
		
		# retrieve asp name (last argument)
		self.aspname = argv[2]
		
	
	def set_cfg_dir(self):
		if self._cfg_dir is None: #if not already set
		  # retrieve environment
		  simphoni = os.getenv('SIMphonIC')
		  if not simphoni:
		    print '$SIMphonIC not set'
		    sys.exit(1)
		  self._cfg_dir=simphoni+'/asp/'+self.aspname+'/mmi/'+self.appliname+'/cfg'	

	def set_cfgfilename(self):
		if self.cfgfilename is None: #user can change the cfg file name
		  self.cfgfilename='mmi_'+self.appliname+'.cfg'

	def run(self,argv):
		
		self.set_appliaspname(argv)
		self.set_cfg_dir()		
		try:
			os.chdir(self._cfg_dir)
		except:
			print 'failed to chdir to %s' % self._cfg_dir
			sys.exit(0)
		self.set_cfgfilename()

		# distinguish between group titles and CfgParameter objects
		self.clean_parameters()
		
		# welcome message
		if self._invite is None: #user can change it
			self._invite= _INVITE % (
					self.appliname,
					self._cfg_dir+'/'+self.cfgfilename )
		print self._invite

		# old cfg file ?
		try:
		        ifile = open(self.cfgfilename,'r')
		except IOError:
		        ifile = None
		        print 'config file does not exist, full configuration required'
		if ifile:
			self.read_config(ifile)
			ifile.close()

		# Prompt user
		try:
			self.edit()
		except KeyboardInterrupt:
		        print
		        print 'aborting configuration, no files were changed'
		        sys.exit(1)

		# write new cfg file
		try:
		        ofile = open(self.cfgfilename+'.new','w')
		except IOError:
		        print '** failed to create new config file, giving up **'
		        sys.exit(1)			
		self.write_config(ofile)
		ofile.close()

		# backup old cfg file
		if ifile:	
			os.rename(self.cfgfilename,self.cfgfilename+'.old')
			print "\nold cfg file renamed %s.old" % self.cfgfilename

		# rename and exit	
		os.rename(self.cfgfilename+'.new',self.cfgfilename)
		print "new cfg file created"
		print "\nMMI configuration successful"
		if not self._farewell: self._farewell=_BYE #user can change it
		print self._farewell	
		sys.exit(0)



class CfgParameter:
	"""
	This class describes a config parameter.
	
	Attributes:

	Mandatory:
	name -- label of the attribute as it will appear in the config file

	Optionnal:
	type -- value in 'string', 'int', 'bool', 'choice' or 'choice_list'.
		Default: 'string'
		For 'choice_list' parameters, several values are allowed. The 
		character '|' (pipe) is used to separate them.
	value -- default parameter value
	range -- for 'int' parameters, specifies the authorised range
		Default: [0,999999]
	max_length -- for 'string' parameters, specifies the string max length
		Default : 100
	choices -- for 'choices' parameters, specifies the authorised choices
		Default: empty list
	prompteur -- string to be displayed to ask the user for the new 
		parameter value. Defaults to parameter name.
	"""
	
	def __init__(self, name, type='string', value=None, **kw):
		self.name = name
		self.type = type
		self.value = value
		self.range = [0,999999]
		self.max_length = 100	
		self.choices = []
		self.prompteur = self.name
		for name, value in kw.items():
                    setattr(self, name, value)
		self.set_pattern()

	def set_pattern(self):
	    pattern_label = '^'+self.name+'[\s]+'
	    if self.type in ('int','integer'): 
		self.type = 'int'
		pattern_value = '(\d+)[\s]*$' #match int
	    elif self.type in ('choice_list','list_choice'):
		self.type='choice_list'
		pattern_value = '([:\/\.\w|]+)$' # match also '|' and ':'
	    else: 
		pattern_value = '([:\/\.\w-]+)[\s]*$' #match /,.,:,letter
	    self.pattern = pattern_label+pattern_value 

	def match(self,line):
	    _p_line = re.compile(self.pattern)
	    #print '******** SEEK  %s **********' % self.name
	    m = _p_line.match(line)
	    if m: 
		#print '*** %s MATCHES !! ***' % self.name
		self.value = m.group(1)
		#print self.name, self.type, self.value, type(self.value,)
		if self.type=='int': 
			self.value = int(self.value)
		if self.type=='choice_list': 
			self.value=string.split(self.value,'|')
	    return m

	def prompte(self):
		if self.type in ('bool','boolean'):
			self.value = _get_bool(
						self.prompteur,
						self.value	)
		if self.type in ('int','integer'):
			if self.value is not None: self.value = int(self.value)
			self.value = _get_int_in_range(	
						self.prompteur,
						self.range[0],
						self.range[1],
						self.value	)
		if self.type in ('string','str'):
			self.value = _get_string(
						self.prompteur,
						self.max_length,
						self.value	)

		if self.type in ('choice','choices'):
			self.value = _get_choice(
						self.prompteur,
						self.choices,
						self.value	) 
		if self.type in ('choice_list','list_choice'):
			self.value = _get_choice_list(
						self.prompteur,
						self.choices,
						self.value	)


# // INTERNALLY USED ---------------------------------------------------------


_blk_line = re.compile(r'^\s*$')
_cmt_line = re.compile(r'^#.*$')


_HEADER = """# do ***NOT*** edit

#########################################################
# file: configuration of mmi %(appliname)s for asp %(aspname)s
# creation: %(time)s 
#########################################################
"""

_INVITE="""
	******************************************************************
	**								**
	** 	          Welcome to the mmi configurator		**
	**								**
	******************************************************************

--------------------------------------------------------------------------------
Configuration of %s
Config file: %s
--------------------------------------------------------------------------------
"""

_BYE="""
	******************************************************************
	**								**
        **	       Thanks for using the mmi configurator		**
	**								**
        ******************************************************************

"""


def _get_int_in_range(prompt,min,max,default):
    ok = None
    if default:
        pr = '%s [%d]: '%(prompt,default)
    else:
        pr = '%s: ' % prompt
    while not ok:
        str = mmi_raw_input(pr)
        if not str: return default
        try:
            val = int(str)
            ok = (val>=min) and (val<=max)
            if not ok: print 'invalid range, min=%d, max=%d' % (min,max)
        except ValueError:
            pass
    return val

def _get_string(prompt,mx_len,default):
    ok = None
    if default:
        pr = '%s [%s]: '%(prompt,default)
    else:
        pr = '%s: ' % prompt
    while not ok:
        str = mmi_raw_input(pr)
        if not str: return default
        ok = len(str)<=mx_len
        if not ok: print 'invalid length, max=%s' % mx_len
    return str

def _get_bool(prompt,default):
    ok = None
    if default:
        pr = '%s [%s]: '%(prompt,default)
    else:
        pr = '%s: ' % prompt
    while not ok:
        str = mmi_raw_input(pr)
        if not str: return default[0] in ['y','Y']
        ok = str[0] in ['y','Y','n','N'] and len(str)==1
        if not ok: print "answer with 'y' or 'n'"
    return str[0] in ['y','Y']

def _get_choice(prompt,allowed,default):
    ok = None
    if default:
        pr = '%s [%s]: '%(prompt,default)
    else:
        pr = '%s: ' % prompt
    while not ok:
        str = mmi_raw_input(pr)
        if not str and default in allowed: 
		return default
        ok = str in allowed
        if not ok:
            print "answer with one of ",
            for l in allowed:
                print '%s ' % l,
            print
    return str

def _get_choice_list(prompt,allowed,default):
	ok=1;l=[]
	max=len(allowed)
	if not isinstance(default,list): default=[default]

	item=_get_choice(prompt+' #1',allowed,default[0])
	allowed.remove(item)
	l.append(item)

	i=1
	while ok and i<max:	
		i=i+1
		if i>len(default): default1='No'
		else: default1='Yes'
		ok=_get_bool('New '+prompt+' (Yes or No)',default1)
		if ok: 
			try: default2=default[i-1]
			except IndexError: default2=''
			if default2 not in allowed: default2=''
			item=_get_choice(prompt+' #'+str(i),allowed,default2)
			allowed.remove(item)
			l.append(item)

	return string.join(l,'|')


def _test():
	mmicfg = MMIConfigurator()
	mmicfg._cfg_dir = '.'
	mmicfg.cfgfilename = 'test_cfg'
	mmicfg.aspname = 'aspname'
	p1 = CfgParameter('param1',type='choice_list', choices=['AAA','BBB','CCC'], value=['CCC','BBB','AAA']) 
	mmicfg.addParameters(p1)
	mmicfg.run(['mmi_appliname_cfg','appliname', 'aspname'])


	print "test ok"

if __name__=="__main__":
        _test()

