#!/usr/bin/env python

import os, sys, pwd
import commands
from mmi_configurator import CfgParameter

#is SIMphonIC defined ?
simphoni=os.getenv('SIMphonIC')
if not simphoni:
        print '$SIMphonIC not set'
        sys.exit(1)

#is HOME defined ?
home=os.getenv('HOME')
if not home:
	print '$HOME not set'
	sys.exit(1)

#ASP ?
asp=commands.getoutput('whoami')

proxy4_mtmoSRV_test_config = [
		'proxy4_mtmoSRV_test parameters'
		]


