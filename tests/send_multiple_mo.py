#!/usr/bin/env python
import os
import sys
import time
import binascii

import Pyro.core
import Pyro.naming
#import SIMphonIC

import md

class MO():
	def __init__(self, msisdn, sme, content, asp, delay = 0):
		self.delay = delay
		self.asp = asp
		self.mo = md.mdIND_Message (
				md_ref = md.mdRef(0, 0),
				origin = md.Address(msisdn, 0),
				destination = md.Address(sme, 0),
				g340_smmo = md.g340SmmoParam(0,
						     0,
						     md.Date(0, 0, 0, 0, 1, 0, 0, 0),
						     0,
						     0),
				u_data = binascii.hexlify(content)
				)
	def post(self, md_proxy, params = {}):
		delay = params.get('delay', 0)
		time.sleep(self.delay)
		com_rc = md_proxy.post(self.mo, self.asp)
#		return com_rc == SIMphonIC.comOK, SIMphonIC.comStatus[com_rc]
#		return com_rc == 0, "%d" % com_rc


if __name__ == '__main__':
	msisdn = sys.argv[3]
	text = sys.argv[4]
	asp = sys.argv[1]
	sme = sys.argv[2]
				
	md_proxy= Pyro.core.getProxyForURI(Pyro.naming.NameServerLocator().getNS().resolve('scenario'))

	for i in range(10):
		mo = MO(msisdn + str(i), sme, text, asp)
		mo.post(md_proxy)
