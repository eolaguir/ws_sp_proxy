#!/usr/bin/env python

import os, sys, pwd
import commands
from mmi_configurator import CfgParameter

#is SIMphonIC defined ?
simphoni=os.getenv('SIMphonIC')
if not simphoni:
        print '$SIMphonIC not set'
        sys.exit(1)

#is HOME defined ?
home=os.getenv('HOME')
if not home:
	print '$HOME not set'
	sys.exit(1)

#ASP ?
asp=commands.getoutput('whoami')

ws_sp_proxy_test_config = [
		'ws_sp_proxy_test parameters'
		]


