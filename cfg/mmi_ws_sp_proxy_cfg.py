#!/usr/bin/env python

import os, sys, pwd
import commands
from mmi_configurator import CfgParameter

#is SIMphonIC defined ?
simphoni=os.getenv('SIMphonIC')
if not simphoni:
        print '$SIMphonIC not set'
        sys.exit(1)

#is HOME defined ?
home=os.getenv('HOME')
if not home:
	print '$HOME not set'
	sys.exit(1)

#ASP ?
asp=commands.getoutput('whoami')


log_path = CfgParameter(	
		name='LOG_PATH',
		prompteur='Log directory ...................... ',
		value='%s/asp/%s/mmi/ws_sp_proxy/logs' % (simphoni,asp))
				
soap_target = CfgParameter(
                name='SOAP_TARGET',
		prompteur='Soap target ? ....................... ',
		value='http://localhost:9393/mockaOcsTelepathIC_PullingProcessWSSoap')

await_mobile_identifier = CfgParameter(
		name='AWAIT_MOBILE_IDENTIFIER',
		prompteur='Mobile identifier ? .........',
		type='choice',
		value='msisdn',
		choices=['msisdn','idtech'] )

error_message_1 = CfgParameter(
		name='SP_HTTP_ERROR_1',
		prompteur='Error message ? .........',
		value='Votre demande ne peut aboutir')

                
ws_sp_proxy_config = [
		'ws_sp_proxy parameters',
		log_path,
		soap_target,
		await_mobile_identifier,
		error_message_1
		]


